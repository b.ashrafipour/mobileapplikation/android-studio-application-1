import me.champeau.gradle.igp.gitRepositories

pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

plugins {
    id("me.champeau.includegit") version "0.1.6"
}

gitRepositories {
    include("vico") {
        uri = "https://github.com/patrykandpatrick/vico.git"
        branch = "candlestick-and-pie-charts"
    }
}


rootProject.name = "MyFirstApp"
include(":app")
 