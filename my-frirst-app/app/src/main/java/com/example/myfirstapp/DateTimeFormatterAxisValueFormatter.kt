package com.example.myfirstapp


import com.patrykandpatrick.vico.core.cartesian.axis.AxisPosition
import com.patrykandpatrick.vico.core.cartesian.axis.AxisValueFormatter
import com.patrykandpatrick.vico.core.cartesian.values.ChartValues
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * An [AxisValueFormatter] implementation that formats LocalDate values using a given [DateTimeFormatter].
 */
class DateTimeFormatterAxisValueFormatter(
    private val dateTimeFormatter: DateTimeFormatter
) : AxisValueFormatter<AxisPosition.Horizontal.Bottom> {

   fun formatValue(value: Any): CharSequence {
        return if (value is LocalDate) {
            dateTimeFormatter.format(value)
        } else {
            throw IllegalArgumentException("Value is not a LocalDate")
        }
    }

    override fun formatValue(
        value: Float,
        chartValues: ChartValues,
        verticalAxisPosition: AxisPosition.Vertical?
    ): CharSequence {


        TODO("Not yet implemented")
    }
}