package com.example.myfirstapp.ui.convert

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myfirstapp.data.Repository
import kotlinx.coroutines.launch


class ConvertViewModel : ViewModel() {
    var state by mutableStateOf(ConvertState())
        private set

    fun setBaseCurrency(from: String) {
        state = state.copy(from = from)
        updateGoalAmount()
    }

    fun setBaseAmount(baseAmount: Double) {
        state = state.copy(baseAmount = baseAmount)
        updateGoalAmount()
    }

    fun setGoalCurrency(to: String) {
        state = state.copy(to = to)
        updateGoalAmount()
    }

    private fun updateGoalAmount() {
        viewModelScope.launch {
            state = state.copy(goalAmount = Repository.latest(
                amount = state.baseAmount,
                from = state.from,
                to = state.to
            ))
        }
    }

    init {
        viewModelScope.launch {
            state = state.copy(currencyMap = Repository.currencyMap())
            setBaseAmount(1.0)

        }
    }



}