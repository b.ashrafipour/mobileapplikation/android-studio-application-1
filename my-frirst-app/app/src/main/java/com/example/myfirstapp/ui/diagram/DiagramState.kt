package com.example.myfirstapp.ui.diagram

import java.time.LocalDate

data class DiagramState(
    val from: String = "EUR",
    val baseAmount: Double = 1.0,
    val to: String = "USD",
    val goalAmount: Double? = null,
    val currencyMap: Map<String,String> = emptyMap(),
    val timeMap: Map<LocalDate, Map<String, Double?>> = emptyMap(),
    val startDate: String = "2000-10-04",
    val duration: String = "1W",
)