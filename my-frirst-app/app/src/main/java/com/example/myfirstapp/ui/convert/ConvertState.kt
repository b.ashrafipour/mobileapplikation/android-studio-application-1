package com.example.myfirstapp.ui.convert

data class ConvertState(
    val from: String = "EUR",
    val baseAmount: Double = 1.0,
    val to: String = "USD",
    val goalAmount: Double? = null,
    val currencyMap: Map<String,String> = emptyMap()
)