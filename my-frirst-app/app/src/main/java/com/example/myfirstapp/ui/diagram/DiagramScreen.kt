package com.example.myfirstapp.ui.diagram


import android.graphics.Color.GREEN
import android.graphics.Color.RED
import android.graphics.Color.WHITE
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.axisLabelComponent
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottomAxis
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStartAxis
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberCandlestickCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.common.scroll.rememberChartScrollState
import com.patrykandpatrick.vico.compose.common.style.ProvideChartStyle
import com.patrykandpatrick.vico.compose.m3.style.m3ChartStyle
import com.patrykandpatrick.vico.core.cartesian.layer.CandlestickCartesianLayer
import com.patrykandpatrick.vico.core.cartesian.layer.CandlestickCartesianLayer.Candle
import com.patrykandpatrick.vico.core.cartesian.model.CandlestickCartesianLayerModel.Entry
import com.patrykandpatrick.vico.core.cartesian.model.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.model.candlestickSeries
import com.patrykandpatrick.vico.core.common.component.LineComponent
import com.patrykandpatrick.vico.core.common.component.textComponent
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit.DAYS

@Composable
fun DiagramScreen(vm: DiagramViewModel) {

    Box(
        modifier = Modifier
            .background(Color(0xD5BDC0C0))
            .padding(horizontal = 2.dp)
    )
    {


        Column(
        modifier = Modifier
            .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        )
        {
            /**
             * currencies signs and amounts
             */
        Column(
            modifier = Modifier
                .padding(top = 15.dp)
                .height(150.dp)
                .clip(RoundedCornerShape(15.dp))
                .background(Color(0xA1587572)),
            horizontalAlignment = Alignment.CenterHorizontally
        )
        {
            if (vm.state.currencyMap.isEmpty())
                CircularProgressIndicator()
            else {
                //base and goal currencies
                Row(
                    modifier = Modifier
                        .padding(top = 25.dp)
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly
                ) {
                    val modifier = Modifier
                        .clip(RoundedCornerShape(10.dp))
                        .size(165.dp, 58.dp)
                        .background(Color(0xBCD5E6E1))
                        .border(3.dp, color = Color(0xFFFFFCFC), RoundedCornerShape(10.dp))

                    //base currency
                    Row(
                        modifier = modifier,
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        CurrencyTextField(vm.state.from)
                        DropDownCurrencies(vm = vm, changeCurrency = vm::setBaseCurrency)
                    }
                    //goal currency
                    Row(
                        modifier = modifier,
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        CurrencyTextField(vm.state.to)
                        DropDownCurrencies(vm = vm, changeCurrency = vm::setGoalCurrency)
                    }
                }

                // text field -> 1 USD = .... EUR
                Column(
                    Modifier
                        .padding(top = 20.dp)
                        .fillMaxWidth()
                        .height(30.dp)
                        .background(Color(0xB0F7F7FC)),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        vm.setBaseAmount(1.0)
                        Text(
                            "1 ${vm.state.from} = ${vm.state.goalAmount} ${vm.state.to}",
                            textAlign = TextAlign.Center
                        )
                    }
            }
        }

            /**
             * Timeline buttons --> 1W, 1M, ...
             */
            TimelineButtons(vm)

            /**
             * Candle-stick-Diagram
             */
        Box(
            modifier = Modifier
                .padding(top = 15.dp)
                .height(250.dp)
                .clip(RoundedCornerShape(22.dp))
                .background(Color(0xFF2A3837))
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            Box(
                modifier = Modifier
                    .padding(20.dp)
            ) {
                if (vm.state.timeMap.isEmpty())
                    CircularProgressIndicator(
                        modifier = Modifier
                            .align(Alignment.Center)
                    )
                else
                    CandleStick(vm.state.timeMap, vm.state.duration)
            }
        }
            /**
             * last candle info
             */
        Column(
            modifier = Modifier
                .padding(top = 15.dp)
                .height(140.dp)
                .fillMaxSize()
                .clip(RoundedCornerShape(22.dp))
                .background(Color(0xBCD5E6E1)),
        )
        {
            val dur: String? = when (vm.state.duration) {
                "1W" -> "week"
                "1M" -> "month"
                "3M" -> "three months"
                "1Y" -> "year"
                "5Y" -> "five years"
                else -> null
            }
            Box(
                modifier = Modifier
                    .padding(top = 15.dp)
                    .fillMaxWidth()
                    .height(40.dp)
                    .background(Color(0xA1587572)),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "last $dur",
                    modifier = Modifier
                        .padding(7.dp),
                    style = TextStyle(
                        color = Color(0xFFE5EEEA),
                        fontSize = 25.sp
                    )
                )
            }
            Column(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .padding(vertical = 6.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    LastInfo(key = "first", title = "open", vm = vm)
                    LastInfo(key = "last", title = "close", vm = vm)
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    LastInfo(key = "min", title = "minimum", vm = vm)
                    LastInfo(key = "max", title = "maximum", vm = vm)
                }
            }
        }
    }
}
}


@Composable
fun DropDownCurrencies(vm: DiagramViewModel, changeCurrency: (String) -> Unit) {
    var expanded by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
            .background(Color(0x1DD5E6E1))
    ) {
        IconButton(onClick = { expanded = true }) {
            Icon(Icons.Default.ArrowDropDown, contentDescription = "Localized description")
        }
        DropdownMenu(
            modifier = Modifier
                .absoluteOffset(0.dp, 0.dp)
                .height(300.dp),
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            vm.state.currencyMap.keys.toList().forEach {
                DropdownMenuItem(
                    text = { Text("$it  (${vm.state.currencyMap.getOrDefault(it,0.0)})") },
                    onClick = {
                        changeCurrency(it)
                        expanded = false
                    },
                )
            }
        }
    }
}

@Composable
fun CurrencyTextField(input: String) {
    Column(
        modifier = Modifier
            .size(115.dp, 50.dp)
            // .padding(start = 5.dp)
            .background(Color(0xBCD5E6E1)),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(input, fontSize = 25.sp)
    }
}

@Composable
fun TimelineButtons(vm: DiagramViewModel) {
    val timeList = listOf("1W", "1M", "3M", "1Y", "5Y")
    Row(
        modifier = Modifier
            .padding(top = 10.dp)
            .clip(RoundedCornerShape(15.dp))
            .background(Color(0xB5ECF1F7))
            .height(50.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.CenterVertically
    ) {
        timeList.forEach {
            Box(
                modifier = Modifier
                    .size(33.dp)
                    .clip(RoundedCornerShape(5.dp))
                    .background(Color(0xA19EC0BD))
                    .clickable {
                        vm.setDuration(it)
                    },
                contentAlignment = Alignment.Center,
            )
            {
                Text(it, fontSize = 15.sp)
            }
        }
    }
}

@Composable
fun CandleStick(dataMap: Map<LocalDate, Map<String, Double?>>, duration: String) {

    val model = dataMap.map { (index, entry) ->
        Entry(
            x = index.toEpochDay(),
            low = entry["min"] ?: 0.0,
            open = entry["last"] ?: 0.0,
            close = entry["first"] ?: 0.0,
            high = entry["max"] ?: 0.0
        )
    }
    val xStep = with(dataMap.keys.take(2)) { DAYS.between(first(), last()) }.toFloat()
    val chartScrollState = rememberChartScrollState()

    val candlestickLayerConfig = CandlestickCartesianLayer.Config(
        absolutelyIncreasingRelativelyIncreasing = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        ),
        absolutelyIncreasingRelativelyZero = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        ),
        absolutelyIncreasingRelativelyDecreasing = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        ),
        absolutelyZeroRelativelyIncreasing = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        ),
        absolutelyZeroRelativelyZero = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        ),
        absolutelyZeroRelativelyDecreasing = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        ),
        absolutelyDecreasingRelativelyIncreasing = Candle(
            realBody = LineComponent(color = RED, thicknessDp = 5f),
            upperWick = LineComponent(color = RED, thicknessDp = 2f),
            lowerWick = LineComponent(color = RED, thicknessDp = 2f)
        ),
        absolutelyDecreasingRelativelyZero = Candle(
            realBody = LineComponent(color = RED, thicknessDp = 5f),
            upperWick = LineComponent(color = RED, thicknessDp = 2f),
            lowerWick = LineComponent(color = RED, thicknessDp = 2f)
        ),
        absolutelyDecreasingRelativelyDecreasing = Candle(
            realBody = LineComponent(color = GREEN, thicknessDp = 5f),
            upperWick = LineComponent(color = GREEN, thicknessDp = 2f),
            lowerWick = LineComponent(color = GREEN, thicknessDp = 2f)
        )
    )
    ProvideChartStyle(m3ChartStyle()) {
        CartesianChartHost(
            chart = rememberCartesianChart(
                rememberCandlestickCartesianLayer(candlestickLayerConfig),
                startAxis = rememberStartAxis(label = axisLabelComponent(
                    color = Color.White
                ),),
                bottomAxis = rememberBottomAxis(
                    valueFormatter = { x, _, _ ->
                        val date = LocalDate.ofEpochDay(x.toLong())
                        when (duration) {
                            "1W" -> date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                            "1M", "3M" -> date.format(DateTimeFormatter.ofPattern("yyyy-MM"))
                            "1Y", "5Y" -> date.format(DateTimeFormatter.ofPattern("yyyy"))
                            else -> ""
                        }
                    },
                    label = textComponent {
                        textSizeSp = MaterialTheme.typography.labelSmall.fontSize.value
                        color= WHITE
                    },
                    labelRotationDegrees = -90f
                ),
            ),
            modelProducer = CartesianChartModelProducer.build { candlestickSeries(model) },
            getXStep = { xStep },
          chartScrollState = chartScrollState
        )
    }
}

@Composable
fun LastInfo(key: String, title: String, vm: DiagramViewModel) {
    Text(
        text = "$title:  " + vm.state.timeMap.entries.lastOrNull()?.value?.getOrDefault(
            key,
            0.0
        )?.let { vm.roundDouble(it) })
}