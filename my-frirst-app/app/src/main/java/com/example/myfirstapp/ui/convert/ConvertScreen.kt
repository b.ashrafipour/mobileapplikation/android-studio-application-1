package com.example.myfirstapp.ui.convert

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.ui.draw.BlurredEdgeTreatment
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.window.Dialog
import androidx.navigation.NavController
import com.example.myfirstapp.R

@Composable
fun ConvertScreen(vm: ConvertViewModel, navController: NavController) {
    var showDialog by remember { mutableStateOf(false) }
    var selectedCurrencies by remember { mutableStateOf(emptyList<String>()) }
    val currencies = (vm.state.currencyMap.keys.toList()).minus(vm.state.from)

    Box(
        modifier = Modifier
            .background(Color(0xD5BDC0C0))
            .width(IntrinsicSize.Max)
    ) {
        Effect()
        Effect2()
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            /**
             * Title of Screen --> Currency Converter
             */
            Box(
                modifier = Modifier
                    .padding(top = 30.dp)
                    .height(60.dp)
                    .fillMaxWidth(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "Currency Converter",
                    style = TextStyle(
                        fontSize = 35.sp,
                        color = Color(0xFF2F4B48),
                        shadow = Shadow(
                            color = Color(0xFFF0EDE9), offset = Offset(9f, 10.0f), blurRadius = 3f
                        )
                    )
                )
            }

            /**
             * Row --> from  'currency name' + dropdown Icon
             */

            Row(
                modifier = Modifier
                    .padding(top = 30.dp, start = 5.dp, end = 5.dp)
                    .clip(RoundedCornerShape(topStart = 15.dp, topEnd = 15.dp))
                    .fillMaxWidth()
                    .height(100.dp)
                    .background(Color(0xBCD5E6E1)),
                verticalAlignment = Alignment.CenterVertically
            ) {

                Box(
                    modifier = Modifier
                        .fillMaxSize(0.5f),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "From",
                        style = TextStyle(
                            fontSize = 25.sp
                        )
                    )
                }
                Row(
                    modifier = Modifier
                        .padding(start = 30.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    CurrencyTextField(vm.state.from)
                    DropDownCurrencies(vm = vm, changeCurrency = vm::setBaseCurrency)
                }
            }
            HorizontalDivider(
                modifier=Modifier
                    .padding(horizontal = 5.dp)
                ,
                thickness = 5.dp,
                color = Color(0xFF91AF9C)

            )

            /**
             * Row --> amount 'input a number' default: 1:0
             */

            Row(
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
                    .fillMaxWidth()
                    .height(80.dp)
                    .clip(
                        RoundedCornerShape(
                            bottomStart = 20.dp,
                            bottomEnd = 20.dp
                        )
                    )
                    .background(Color(0xA9E5F0EF)),
                verticalAlignment = Alignment.CenterVertically

            )
            {
                //Amount
                Box(
                    modifier = Modifier
                        .fillMaxSize(0.5f),
                    //   .background(Color.Red),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "Amount",
                        style = TextStyle(
                            fontSize = 25.sp
                        )
                    )
                }
                AmountTextField(value = vm.state.baseAmount, setBaseAmount = vm::setBaseAmount)
            }

            //exchange Icon
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(90.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                Box(
                    modifier = Modifier
                        .padding(5.dp)
                        .size(80.dp)
                        .clip(RoundedCornerShape(50.dp))
                        .background(Color(0xD0C4E9D2))
                        .border(1.dp, Color.Black, RoundedCornerShape(50.dp)),
                    contentAlignment = Alignment.Center
                ) {
                    Icon(
                        modifier = Modifier
                            .size(50.dp),
                        painter = painterResource(
                            id = R.drawable.repost
                        ),
                        contentDescription = "exchange"
                    )
                }
            }


            /**
             * Row --> to  'currency name' + dropdown Icon
             */

            Row(
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
                    .clip(RoundedCornerShape(topStart = 15.dp, topEnd = 15.dp))
                    .fillMaxWidth()
                    .height(100.dp)
                    .background(Color(0xBCD5E6E1)),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize(0.5f),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "To",
                        style = TextStyle(
                            fontSize = 25.sp
                        )
                    )
                }
                Row(
                    modifier = Modifier
                        .padding(start = 30.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    CurrencyTextField(vm.state.to)
                    DropDownCurrencies(vm = vm, changeCurrency = vm::setGoalCurrency)
                }
            }
            HorizontalDivider(
                modifier=Modifier
                    .padding(horizontal = 5.dp),
                thickness = 5.dp,
                color = Color(0xFF91AF9C)
            )

            /**
             * Row --> amount
             */

            Row(
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
                    .fillMaxWidth()
                    .height(80.dp)
                    .clip(
                        RoundedCornerShape(
                            bottomStart = 20.dp,
                            bottomEnd = 20.dp
                        )
                    )
                    .background(Color(0xA9E5F0EF)),
                verticalAlignment = Alignment.CenterVertically
            )
            {
                //Amount
                Box(
                    modifier = Modifier
                        .fillMaxSize(0.5f),
                    //   .background(Color.Red),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "Amount",
                        style = TextStyle(
                            fontSize = 25.sp
                        )
                    )
                }
                CurrencyTextField(input = "${vm.state.goalAmount}")
            }

            /**
             * a text button  --> opens a dialog box of currency list to choose any
             */

            Column(
                modifier = Modifier
                    .padding(top = 10.dp)
                    .fillMaxWidth()
                    .height(100.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                TextButton(
                    modifier = Modifier
                        .size(450.dp, 80.dp)
                        .padding(12.dp),
                    onClick = { showDialog = true },
                ) {
                    Text(
                        text = "select more currencies",
                        fontSize = 22.sp,
                        color = Color.White
                    )
                }
                if (showDialog) {
                    CurrencySelectionDialog(
                        vm = vm,
                        currencies = currencies,
                        onDismissRequest = { showDialog = false },
                        onSelectionConfirmed = {
                            selectedCurrencies = it
                            showDialog = false // Close the dialog
                            navController.navigate("list/${selectedCurrencies.joinToString(",")}/${vm.state.from}")
                        },
                    )
                }
            }
        }
    }
}


@Composable
fun CurrencyTextField(input: String) {
    Column(
        modifier = Modifier
            .padding(start = 20.dp)
            .clip(RoundedCornerShape(5.dp)),
        verticalArrangement = Arrangement.Center,
    ) {
        Text(input, fontSize = 40.sp, color = Color.Black)
    }
}


@Composable
fun DropDownCurrencies(vm: ConvertViewModel, changeCurrency: (String) -> Unit) {
    var expanded by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.CenterStart)
    ) {
        IconButton(onClick = { expanded = true }) {
            Icon(Icons.Default.ArrowDropDown, contentDescription = "Localized description")
        }
        DropdownMenu(
            modifier = Modifier
                .absoluteOffset(0.dp, 0.dp)
                .height(300.dp),
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            vm.state.currencyMap.keys.toList().forEach {
                DropdownMenuItem(
                    text = { Text("$it  (${vm.state.currencyMap.getOrDefault(it, 0.0)})") },
                    onClick = {
                        changeCurrency(it)
                        expanded = false
                    },
                )
            }
        }
    }
}

@Composable
fun AmountTextField(value: Double?, setBaseAmount: (Double) -> Unit) {
    OutlinedTextField(
        modifier = Modifier
            .fillMaxSize(),
        textStyle = TextStyle(
            textAlign = TextAlign.Center,
            fontSize = 35.sp
        ),
        value = "$value",
        onValueChange = { setBaseAmount(it.toDoubleOrNull() ?: 1.0) },
        colors = OutlinedTextFieldDefaults.colors(
            focusedBorderColor = Color.Transparent,
            unfocusedBorderColor = Color.Transparent,
            disabledBorderColor = Color.Transparent,
            errorBorderColor = Color.Transparent
        )
    )
}

@Composable
fun Effect() {
    Box(
        modifier = Modifier
            .rotate(25f)
            .background(
                Brush.linearGradient(
                    0.0f to Color(0xD5696969),
                    400.0f to Color(0xD5BDC0C0),
                    start = Offset.Zero,
                    end = Offset.Infinite
                )
            )
            .size(1000.dp, 1000.dp)
            .blur(
                radiusX = 20.dp,
                radiusY = 3.dp,
                edgeTreatment = BlurredEdgeTreatment(RoundedCornerShape(5.dp))
            ),
    )
}

@Composable
fun Effect2() {
    Box(
        modifier = Modifier
            .rotate(-25f)
            .background(
                Brush.linearGradient(
                    0.0f to Color(0xD5BDC0C0),
                    600.0f to Color(0xD5696969),
                    start = Offset.Zero,
                    end = Offset.Infinite
                )
            )
            .size(1000.dp, 1000.dp)
            .blur(
                radiusX = 20.dp,
                radiusY = 3.dp,
                edgeTreatment = BlurredEdgeTreatment(RoundedCornerShape(9.dp))
            )
    )
}

@Composable
fun CurrencySelectionDialog(
    vm: ConvertViewModel,
    currencies: List<String>,
    onDismissRequest: () -> Unit,
    onSelectionConfirmed: (List<String>) -> Unit
) {
    var selectedCurrencies by remember { mutableStateOf(emptyList<String>()) }

    Dialog(onDismissRequest = { onDismissRequest() }) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .clip(RoundedCornerShape(20.dp))
                .background(Color(0xE1E1E2E2))
        ) {
            Text(
                text = "Select Currencies",
                style = TextStyle(
                    fontSize = 25.sp,
                    shadow = Shadow(
                        color = Color(0xFFFFFFFF)
                    )
                ),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
                    .background(Color(0xD5585858))
                    .padding(top = 15.dp),
                color = Color(0xD5D1D3D3)
            )
            LazyColumn(
                modifier = Modifier.weight(1f)
            ) {
                items(currencies) { currency ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Checkbox(
                            checked = selectedCurrencies.contains(currency),
                            onCheckedChange = { isChecked ->
                                selectedCurrencies = if (isChecked) {
                                    selectedCurrencies + currency
                                } else {
                                    selectedCurrencies - currency
                                }
                            },
                            colors = CheckboxDefaults.colors(
                                checkmarkColor = Color.Green
                            )
                        )
                        Text(
                            text = "$currency    (${vm.state.currencyMap.getOrDefault(currency,0.0)})",
                            style = MaterialTheme.typography.bodyLarge,
                            modifier = Modifier.padding(start = 8.dp)
                        )
                    }
                }
            }
            Button(
                onClick = {
                    onSelectionConfirmed(selectedCurrencies)
                },
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
            ) {
                Text("OK")
            }
        }
    }
}
