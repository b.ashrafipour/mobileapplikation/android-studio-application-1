package com.example.myfirstapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.paddingFromBaseline
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.myfirstapp.ui.convert.ConvertScreen
import com.example.myfirstapp.ui.currencyList.CurrencyListScreen
import com.example.myfirstapp.ui.diagram.DiagramScreen
import com.example.myfirstapp.ui.main.MainScreen
import com.example.myfirstapp.ui.theme.MyFirstAppTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyFirstAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "main") {

                        composable(route = "main") {
                            MainScreen(vm = viewModel(),navController)
                        }

                        composable(route = "convert") {
                            ConvertScreen(
                                vm = viewModel(), navController = navController
                            )
                            BottomBar(navController)
                        }

                        composable(route = "diagram") {
                            DiagramScreen(vm = viewModel())
                            BottomBar(navController)
                        }

                        composable(route = "list/{selectedCurrencies}/{title}") { backStackEntry ->
                            val selectedCurrencies =
                                backStackEntry.arguments?.getString("selectedCurrencies")
                                    ?.split(",") ?: emptyList()
                            val title = backStackEntry.arguments?.getString("title") ?: ""

                            CurrencyListScreen(
                                vm = viewModel(),
                                selectedCurrencies = selectedCurrencies,
                                title = title
                            )
                            BottomBar(navController)
                        }
                    }
                }
            }
        }
    }
}


@Composable
fun BottomBar(navController: NavController) {
    val icons = listOf(R.drawable.exchange, R.drawable.home, R.drawable.stock)
    val routes = listOf("convert", "main", "diagram")

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .paddingFromBaseline(bottom = 100.dp),
        horizontalArrangement = Arrangement.SpaceEvenly,
        //verticalAlignment = Alignment.CenterVertically
    ) {

        icons.forEach {
            FloatingActionButton(
                onClick = { navController.navigate(routes[icons.indexOf(it)]) },
                shape = RoundedCornerShape(32.dp),
                modifier = Modifier
                    .size(80.dp),
                containerColor = Color(0xA9DFE3E6)
            ) {
                Icon(
                    modifier = Modifier
                        .size(50.dp),
                    painter = painterResource(it),
                    contentDescription = "button"
                )
            }
        }


    }
}