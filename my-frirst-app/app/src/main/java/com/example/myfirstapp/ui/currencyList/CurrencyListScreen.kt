package com.example.myfirstapp.ui.currencyList

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myfirstapp.R
import com.example.myfirstapp.ui.convert.Effect
import com.example.myfirstapp.ui.convert.Effect2


@Composable
fun CurrencyListScreen(vm: CurrencyListViewModel, selectedCurrencies: List<String>, title: String,) {
    vm.setSelectedCurrencyList(selectedCurrencies)
    vm.setTitle(title)
    Box(
        modifier = Modifier
            .background(Color(0xD5BDC0C0))
            .padding(horizontal = 15.dp),
        ) {
        Effect()
        Effect2()

        Column(
            modifier = Modifier
        ) {

            /**
             * Title --> 1 + currency sign
             */
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
                    .clip(RoundedCornerShape(45.dp))
                    .border(2.dp, Color(0x941D5658), RoundedCornerShape(45.dp))
                    .background(Color(0x4D1D5658)),
                contentAlignment = Alignment.Center
            ) {
                vm.state.title?.let {
                    Text(
                        text = "1 $it",
                        fontSize = 45.sp,
                        modifier = Modifier.padding(10.dp),
                        textAlign = TextAlign.Center,
                        fontFamily = FontFamily.SansSerif,
                        color = Color(0xFF0B2C29)
                    )
                }
            }

            //divider line
            HorizontalDivider(
                modifier=Modifier
                    .padding(top = 20.dp),
                thickness = 3.dp,
                color = Color(0xFF69786C)
            )

            /**
             * selected currency list
             */
            LazyColumn(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .padding(bottom = 80.dp, top = 20.dp),
                verticalArrangement = Arrangement.SpaceEvenly
            )
            {
                items(vm.state.selectedCurrencyList) {
                    Row(
                        modifier = Modifier
                            .padding(2.dp)
                            .fillMaxWidth()
                            .clip(RoundedCornerShape(15.dp))
                            .background(Color(0x651D5658)),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceEvenly
                    ) {
                        val modifier = Modifier
                            .padding(vertical = 6.dp)
                            .size(135.dp, 50.dp)
                            .padding(horizontal = 5.dp)
                            .clip(RoundedCornerShape(15.dp))
                            .border(5.dp, Color(0xD8FAF8F8), RoundedCornerShape(15.dp))
                            .background(Color(0x94607E7B))

                        //from
                        Box(
                            modifier = modifier,
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = it,
                                modifier = Modifier.padding(10.dp),
                                color = Color(0xFFF4F7F6)
                            )
                        }

                        //icon
                        Box(
                            modifier = Modifier
                                .padding(top = 1.dp)
                                .size(55.dp)
                                .clip(RoundedCornerShape(40.dp))
                                .background(Color(0xD8FAF8F8)),
                            contentAlignment = Alignment.Center
                        ) {

                            Icon(
                                modifier = Modifier
                                    .size(45.dp),
                                painter = painterResource(id = R.drawable.rightarrow),
                                contentDescription = "to"
                            )
                        }

                        //to
                        Box(
                            modifier = modifier,
                            contentAlignment = Alignment.Center

                        ) {
                            Text(
                                text = vm.state.currencyMap?.getOrDefault(it, 0.0).toString(),
                                textAlign = TextAlign.Center,
                                color = Color(0xFFF4F7F6)
                            )

                        }
                    }
                }
            }
        }
    }
}

