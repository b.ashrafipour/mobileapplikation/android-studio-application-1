package com.example.myfirstapp.ui.currencyList

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myfirstapp.data.Repository
import kotlinx.coroutines.launch

class CurrencyListViewModel : ViewModel() {
    var state by mutableStateOf(CurrencyListState())
        private set

    fun setSelectedCurrencyList(list: List<String>) {
        state = state.copy(selectedCurrencyList = list)
    }

    fun setTitle(title: String) {
        state = state.copy(title = title)
    }

    init {
        viewModelScope.launch {
            state = state.title?.let { Repository.latestList(1.0, it) }
                ?.let { state.copy(currencyMap = it) }!!
        }
    }


}