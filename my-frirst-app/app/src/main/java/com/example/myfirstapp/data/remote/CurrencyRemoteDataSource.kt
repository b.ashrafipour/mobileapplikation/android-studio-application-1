package com.example.myfirstapp.data.remote


import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.serialization.gson.gson

class CurrencyRemoteDataSource : CurrencyAPI {

    //latest amount of a currency with parameters: from, to, amount  --> in ConvertScreen and DiagramScreen
    override suspend fun latest(amount: Double, from: String, to: String): Double {
        return if (to != from) {
            http.get("latest") {
                parameter("amount", amount)
                parameter("from", from)
                parameter("to", to)
            }.body<Currency>().rates.getValue(to)
        } else {
            1.0
        }
    }

    override suspend fun latestList( amount: Double,from: String): Map<String, Double> {
        return http.get("latest") {
            parameter("amount", amount)
            parameter("from", from)
        }.body<Currency>().rates
    }


    //receive the amount of a currency in a specific date with parameters: date, from, to, amount  --> in DiagramScreen
    suspend fun currencyWithDate(date: String, amount: Double, from: String, to: String): Double {
        return if (to != from) {
            http.get(date) {
                parameter("amount", amount)
                parameter("from", from)
                parameter("to", to)
            }.body<Currency>().rates.getValue(to)
        } else {
            1.0
        }
    }

    //get amount o a currency in a specific period of time with parameters: start_date, end_date, from, to, amount --> DiagramScreen
    override suspend fun historyOfCurrency(
        start: String,
        end: String,
        amount: Double,
        from: String,
        to: String
    ): Map<String, Double> {
        val newMap = mutableMapOf<String, Double>()
        http.get("$start..$end") {
            parameter("amount", amount)
            parameter("from", from)
        }.body<HistoryCurrency>().rates.forEach { (date, rates) ->
            newMap[date] = rates[to] ?: error("Currency $to not found for date $date")
        }
        return newMap
    }

    override suspend fun currencyMap(): Map<String, String> =
        http.get("currencies").body<Map<String, String>>().toMap()


     private val http by lazy {
        HttpClient(Android) {
            install(ContentNegotiation) { gson() }
            defaultRequest { url("https://api.frankfurter.app/") }
        }
    }
}