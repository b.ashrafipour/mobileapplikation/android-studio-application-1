package com.example.myfirstapp.data.remote


// Schnittstelle für die API zum Abrufen von Währungsdaten
interface CurrencyAPI {
    suspend fun latest(amount: Double, from: String, to: String): Double
    suspend fun latestList( amount: Double,from: String): Map<String, Double>
    suspend fun historyOfCurrency(
        start: String,
        end: String,
        amount: Double,
        from: String,
        to: String
    ): Map<String, Double>
    suspend fun currencyMap(): Map<String, String>
}