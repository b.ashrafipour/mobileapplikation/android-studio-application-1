package com.example.myfirstapp.ui.diagram

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import com.example.myfirstapp.data.Repository
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.ChronoField
import java.time.temporal.ChronoField.DAY_OF_YEAR
import java.time.temporal.ChronoField.YEAR
import java.util.Locale

class DiagramViewModel(
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    @OptIn(SavedStateHandleSaveableApi::class)
    var state by savedStateHandle.saveable { mutableStateOf(DiagramState()) }

    fun setBaseCurrency(from: String) {
        state = state.copy(from = from)
        updateGoalAmount()
        viewModelScope.launch {
            printValuesInADuration(state.duration)
        }
    }

    fun setBaseAmount(baseAmount: Double) {
        state = state.copy(baseAmount = baseAmount)
        updateGoalAmount()
    }

    fun setGoalCurrency(to: String) {
        state = state.copy(to = to)
        updateGoalAmount()
        viewModelScope.launch {
            printValuesInADuration(state.duration)
        }
    }

    fun setDuration(duration: String) {
        state = state.copy(duration = duration)
        viewModelScope.launch {
            printValuesInADuration(duration)
        }
    }

    private fun updateGoalAmount() {
        viewModelScope.launch {
            state = state.copy(
                goalAmount = Repository.latest(
                    amount = state.baseAmount,
                    from = state.from,
                    to = state.to
                )
            )
        }
    }

    private suspend fun printValuesInADuration(
        duration: String = state.duration,
    ) {
        val offsetYear = (state.startDate).substring(0..3).toInt() % 5
        val result = when (duration) {
            "1W" -> groupDates { with(ChronoField.DAY_OF_WEEK, 1) }
            "1M" -> groupDates { with(ChronoField.DAY_OF_MONTH, 1) }
            "3M" -> groupDates {
                val yearMonth = YearMonth.from(this)
                val quarter = ((yearMonth.monthValue - 1) / 3) + 1
                YearMonth.of(yearMonth.year, ((quarter - 1) * 3) + 1).atDay(1)}
            "1Y" -> groupDates { with(DAY_OF_YEAR, 1) }
            "5Y" -> groupDates {
                with(YEAR, ((year / 5) * 5 + offsetYear).toLong())
                    .with(DAY_OF_YEAR, 1)
            }
            else -> null
        }
      state = state.copy(timeMap = result ?: emptyMap())
    }

    private suspend fun myMap():Map<String,Double>{
        var start: String = state.startDate
        if (state.duration == "1W") start = LocalDate.now().minusYears(1L).plusDays(1L).toString()
       return Repository.historyOfCurrency(
           start = start,
            end = LocalDate.now().toString(),
            amount = 1.0,
            from = state.from,
            to = state.to
        )
    }

    private suspend fun groupDates(keySelector: LocalDate.() -> LocalDate) =
        myMap().entries
            .groupBy({ (k, _) -> LocalDate.parse(k).keySelector() }, { it.value })
            .mapValues { (_, values) ->
                mapOf(
                    "min" to values.minOrNull(),
                    "max" to values.maxOrNull(),
                    "first" to values.firstOrNull(),
                    "last" to values.lastOrNull()
                )
            }

    fun roundDouble(value: Double): Double {
        val symbols = DecimalFormatSymbols(Locale.getDefault())
        symbols.decimalSeparator = '.'
        val df = DecimalFormat("#.###", symbols)
        return df.format(value).toDouble()
    }

    init {
        viewModelScope.launch {
            state = state.copy(currencyMap = Repository.currencyMap())
            printValuesInADuration()
        }
    }
}