package com.example.myfirstapp.data.remote



class HistoryCurrency(
    val amount: Double,
    val base: String,
    val start_date: String,
    val end_date: String,
    val rates: Map<String,Map<String, Double>>
)