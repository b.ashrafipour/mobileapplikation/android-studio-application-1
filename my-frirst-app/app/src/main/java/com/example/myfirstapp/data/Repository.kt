package com.example.myfirstapp.data

import com.example.myfirstapp.data.remote.CurrencyAPI
import com.example.myfirstapp.data.remote.CurrencyRemoteDataSource

object Repository:
        CurrencyAPI by CurrencyRemoteDataSource()