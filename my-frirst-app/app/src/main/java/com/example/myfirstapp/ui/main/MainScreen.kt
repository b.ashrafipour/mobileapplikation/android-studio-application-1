package com.example.myfirstapp.ui.main

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myfirstapp.R
import com.example.myfirstapp.ui.convert.Effect
import com.example.myfirstapp.ui.convert.Effect2
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottomAxis
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStartAxis
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.common.style.ProvideChartStyle
import com.patrykandpatrick.vico.compose.m3.style.m3ChartStyle
import com.patrykandpatrick.vico.core.cartesian.model.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.model.lineSeries
import com.patrykandpatrick.vico.core.cartesian.values.AxisValueOverrider
import com.patrykandpatrick.vico.core.common.component.textComponent
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@Composable
fun MainScreen(vm: MainViewModel, navController: NavController) {
    Box(
        modifier = Modifier
            .background(Color(0xBFCBC9D1)),
        contentAlignment = Alignment.Center
    ) {
        Effect()
        Effect2()
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {

            /**
             * Box --> Image --> Logo
              */
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(160.dp)
                    .background(Color(0x74A1B3B3)),
                contentAlignment = Alignment.Center
            )
            {
                Image(
                    modifier = Modifier
                        .size(140.dp),
                    painter = painterResource(
                        id = R.drawable.mylogo
                    ),
                    contentDescription = "mainBackground",
                    contentScale = ContentScale.Crop,
                    alpha = 0.4f
                )
            }

            /**
             * Column --> currencies + chart
             */
            Column(
                modifier = Modifier
                    .clip(RoundedCornerShape(20.dp))
                    .fillMaxWidth()
                    .height(320.dp)
                    .background(Color(0xA9DCE2E7))
            )
            {

                //currencies
                Row(
                    modifier = Modifier
                        .padding(2.dp)
                        .fillMaxWidth()
                        .clip(RoundedCornerShape(15.dp))
                        .background(Color(0x651D5658)),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceEvenly

                ) {
                    val modifier = Modifier
                        .padding(vertical = 10.dp)
                        .size(145.dp, 60.dp)
                        .clip(RoundedCornerShape(15.dp))
                        .border(2.dp, Color(0xD8FAF8F8), RoundedCornerShape(15.dp))
                        .background(Color(0x94607E7B))

                    //from
                    Row(
                        modifier = modifier,
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = vm.state.from,
                            modifier = Modifier
                                .padding(15.dp),
                            color = Color(0xFFF4F7F6),
                            style = TextStyle(
                                fontSize = 20.sp
                            )
                        )
                        DropDownCurrencies(vm = vm, vm::setBaseCurrency)
                    }

                    //icon
                    Box(
                        modifier = Modifier
                            .size(80.dp)
                            .background(Color(0xFFCDD5D5), CircleShape),
                        contentAlignment = Alignment.Center
                    ) {
                        Icon(
                            modifier = Modifier
                                .size(60.dp),
                            painter = painterResource(id = R.drawable.rightarrow),
                            contentDescription = "to"
                        )
                    }

                    //to
                   Row(
                       modifier = modifier,
                       verticalAlignment = Alignment.CenterVertically,
                       horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = vm.state.to,
                            modifier = Modifier
                                .padding(15.dp),
                            textAlign = TextAlign.Center,
                            color = Color(0xFFF4F7F6),
                            style = TextStyle(
                                fontSize = 20.sp
                            )
                        )
                       DropDownCurrencies(vm = vm, vm::setGoalCurrency)
                    }
                }

                //diagram
                Box(
                    modifier = Modifier
                        .padding(top = 25.dp)
                        .padding(horizontal = 25.dp),
                    contentAlignment = Alignment.Center
                )
                {
                    if (vm.state.last2MonthDaily?.isEmpty() == true){
                        CircularProgressIndicator(
                            modifier = Modifier
                                .align(Alignment.Center)
                        )
                    }else{
                        vm.state.last2MonthDaily?.let { Charts(dataMap = it) }
                    }
                }
            }

            /**
             * link to convert screen and diagram screen
             */

            LinkTo(icon = R.drawable.exchange, text = "convert currencies"){
                navController.navigate("convert")
            }

            LinkTo(icon = R.drawable.stock, text = "show other charts"){
                navController.navigate("diagram")
            }
        }
    }
}


@Composable
fun LinkTo(icon: Int, text: String, navigate: () -> Unit) {
    Row(
        modifier = Modifier
            .padding(top = 30.dp)
            .size(350.dp, 90.dp)
            .clip(
                RoundedCornerShape(
                    50.dp
                )
            )
            .border(
                2.dp, Color(0xBAE8F5E7), RoundedCornerShape(
                    50.dp
                )
            )
            .background(Color(0x442C7D80))
            .clickable { navigate() },
        verticalAlignment = Alignment.CenterVertically
    ) {
        //Icon
        Box(
            modifier = Modifier
                .padding(start = 10.dp)
                .size(70.dp)
                .clip(RoundedCornerShape(50.dp)),
            contentAlignment = Alignment.Center
        )
        {
            FloatingActionButton(
                onClick = { navigate() },
                shape = RoundedCornerShape(30.dp),
                modifier = Modifier
                    .size(70.dp),
                containerColor = Color(0xA9DEEEF8)
            ) {
                Icon(
                    modifier = Modifier
                        .size(40.dp),
                    painter = painterResource(icon),
                    contentDescription = "button"
                )
            }
        }
        //text
        Box(
            modifier = Modifier
                .fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = text,
                textAlign = TextAlign.Center,
                fontSize = 23.sp,
                fontFamily = FontFamily.Serif,
                color = Color(0xFFEAEEF5),
                style = TextStyle(
                    shadow = Shadow(
                        color = Color(0xFF103D66), offset = Offset(0.5f,5.0f),
                    )
                ),
            )
        }
    }
}

@Composable
fun Charts(dataMap: Map<String, Double>) {
    ProvideChartStyle(m3ChartStyle()) {
        CartesianChartHost(
            chart = rememberCartesianChart(
                rememberLineCartesianLayer(
                    axisValueOverrider = AxisValueOverrider.fixed(
                        minY = dataMap.values.min().toFloat(),
                        maxY = dataMap.values.max().toFloat()
                    )
                ),
                startAxis = rememberStartAxis(),
                bottomAxis = rememberBottomAxis(
                    valueFormatter = { x, _, _ ->
                        val myDate = LocalDate.ofEpochDay(x.toLong())
                        val formattedDate = myDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                        Log.d("Charts", "x: $x, formattedDate: $formattedDate")
                        formattedDate
                    },
                    label = textComponent {
                        textSizeSp = MaterialTheme.typography.labelSmall.fontSize.value
                    },
                    labelRotationDegrees = -90.0f
                )
            ),
            modelProducer = CartesianChartModelProducer.build {
                lineSeries {
                    series(
                        x = dataMap.keys.map { LocalDate.parse(it).toEpochDay() },
                        y = dataMap.values
                    )
                }
            }
        )
    }
}

@Composable
fun DropDownCurrencies(vm: MainViewModel, changeCurrency: (String) -> Unit) {
    var expanded by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
            .background(Color(0x28617E6F))
    ) {
        IconButton(onClick = { expanded = true }) {
            Icon(Icons.Default.ArrowDropDown, contentDescription = "Localized description")
        }
        DropdownMenu(
            modifier = Modifier
                .absoluteOffset(0.dp, 0.dp)
                .height(300.dp),
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            vm.state.currencyMap.keys.toList().forEach {
                DropdownMenuItem(
                    text = { Text("$it  (${vm.state.currencyMap.getOrDefault(it, 0.0)})") },
                    onClick = {
                        changeCurrency(it)
                        expanded = false
                    },
                )
            }
        }
    }
}







