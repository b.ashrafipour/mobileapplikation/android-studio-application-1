package com.example.myfirstapp.ui.currencyList

data class CurrencyListState(
    val currencyMap: Map<String,Double> ? = emptyMap(),
    val title: String? = "EUR",
    val selectedCurrencyList:List<String> = emptyList()
)