import com.example.myfirstapp.data.Repository
import com.example.myfirstapp.data.remote.CurrencyRemoteDataSource
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.ChronoField.DAY_OF_MONTH
import java.time.temporal.ChronoField.DAY_OF_WEEK
import java.time.temporal.ChronoField.DAY_OF_YEAR
import java.time.temporal.ChronoField.YEAR
import java.util.Locale

suspend fun main() {


    println("\n----myMap-----\n")
    val myMap =
        CurrencyRemoteDataSource().historyOfCurrency("2000-01-01", "2024-03-01", 1.0, "EUR", "AUD")
    println(myMap)

    println("\n----myList-----\n")

    val myList = myMap.keys.toList()
    println(myList)


    println("\n${"-".repeat(25)}\n")

    fun groupDates(keySelector: LocalDate.() -> LocalDate) =
        myMap.entries
            .groupBy({ (k, _) -> LocalDate.parse(k).keySelector() }, { it.value })
            .mapValues { (_, values) ->
                mapOf(
                    "min" to values.minOrNull(),
                    "max" to values.maxOrNull(),
                    "first" to values.firstOrNull(),
                    "last" to values.lastOrNull()
                )
            }

    println(groupDates { with(DAY_OF_WEEK, 1) })

    println("\n${"-".repeat(25)}\n")

    println(groupDates { with(DAY_OF_MONTH, 1) })

    println("\n${"-".repeat(25)}\n")

    println(groupDates { with(DAY_OF_YEAR, 1) })

    println("\n${"-".repeat(25)}\n")
    println("---------------3month-------------------")

    val offsetMonth = ("2000-01-01").substring(5..6).toInt() % 3

   // println(groupDates {
   //     with(ChronoField.MONTH_OF_YEAR, (((monthValue / 3) * 3 + offsetMonth)).toLong()).with(DAY_OF_YEAR, 1)})

    println(groupDates {
    val yearMonth = YearMonth.from(this)
    val quarter = ((yearMonth.monthValue - 1) / 3) + 1
    YearMonth.of(yearMonth.year, ((quarter - 1) * 3) + 1).atDay(1)})

    println(offsetMonth)
    println(offsetMonth % 4)

    println("\n${"-".repeat(25)}\n")
    println("---------------5year-------------------")



    val offsetYear = ("2000-01-01").substring(0..3).toInt() % 5

    println(groupDates {
        with(YEAR, ((year / 5) * 5 + offsetYear).toLong()).with(DAY_OF_YEAR, 1)})

    println("2000-12-12".substring(0, 4))

    println(LocalDate.now())
    println(LocalDate.now().minusYears(1).plusDays(1))

    println("\n${"-".repeat(25)}\n")
    println("---------------datamap-------------------")
    val dataMap = mapOf(
        "2024-01-02" to 5.1,
        "2024-01-03" to 1.8,
        "2024-01-04" to 1.0,
        "2024-01-05" to 2.1,
        "2024-01-08" to 2.5,
        "2024-01-09" to 1.6
    )
    println("datamap: $dataMap")

    println(dataMap.minus("2024-01-02"))

    println("datamap: $dataMap")

    println("\n${"-".repeat(25)}\n")
    println("---------------last2MonthDaily-------------------")
    println(last2MonthDaily())
}

    suspend fun myMap():Map<String,Double>{

        return Repository.historyOfCurrency(
            start = "2000-11-12",
            end = LocalDate.now().toString(),
            amount = 1.0,
            from = "EUR",
            to = "AUD")
    }


fun roundDouble(value: Double): Double {
    val symbols = DecimalFormatSymbols(Locale.getDefault())
    symbols.decimalSeparator = '.'
    val df = DecimalFormat("#.##", symbols) // Hier werden die Symbole übergeben
    return df.format(value).toDouble()
}

suspend fun last2MonthDaily():Map<String,Double>{
    return Repository.historyOfCurrency("2024-01-01","2024-03-01",1.0,"EUR","USD")
}