package com.example.myfirstapp.ui.main

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myfirstapp.data.Repository
import kotlinx.coroutines.launch
import java.time.LocalDate

class MainViewModel : ViewModel(){
    var state by mutableStateOf(MainState())
        private set

    fun setBaseCurrency(from: String) {
        state = state.copy(from = from)
        updateGoalAmount()
    }

    fun setGoalCurrency(to: String) {
        state = state.copy(to = to)
        updateGoalAmount()
    }

    private fun updateGoalAmount() {
        viewModelScope.launch {
            state = state.copy(
                goalAmount = Repository.latest(
                    amount = state.baseAmount,
                    from = state.from,
                    to = state.to
                ),
                last2MonthDaily = Repository.historyOfCurrency(
                    state.startDate,
                    LocalDate.now().toString(),
                    1.0,
                    state.from,
                    state.to
                )

            )
        }
    }

    init {
        viewModelScope.launch {
            state = state.copy(
                currencyMap = Repository.currencyMap(),
                last2MonthDaily = Repository.historyOfCurrency(
                    state.startDate,
                    LocalDate.now().toString(),
                    1.0,
                    state.from,
                    state.to
                )
            )
        }
    }
}