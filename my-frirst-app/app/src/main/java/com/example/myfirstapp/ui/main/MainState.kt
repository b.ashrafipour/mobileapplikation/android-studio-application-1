package com.example.myfirstapp.ui.main

import java.time.LocalDate

data class MainState(
   val from: String = "EUR",
   val to: String = "USD",
   val baseAmount: Double = 1.0,
   val goalAmount: Double? = null,
   val currencyMap: Map<String, String> = emptyMap(),
   val startDate: String = LocalDate.now().minusMonths(2L).toString(),
   val last2MonthDaily: Map<String, Double>? = emptyMap()
)